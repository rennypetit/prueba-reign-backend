# Backend

**Autor:** Renny Petit
Proyecto desarrollado para la empresa Regin: https://www.reign.cl/es/

## Documentación

El proyecto esta desarrollado con nodejs, nestjs y mongodb como base de datos. Asimismo se creó una imagen de docker en la cual se conectaron los contenedores de: Backend, Database, Frontend.

De igual modo se realiza el despliegue en gitlab, se corre el CI de pipelines, se ejecuta la instalación de las dependencias y el eslint.

**URL**: https://gitlab.com/rennypetit/prueba-reign-backend

### Ambiente desarrollo

Utiliza sus propias variables de entorno, Dockerfile y docker-compose.

#### Ejecutar

```bash
 docker-compose up
```

### Ambiente de pruebas

Utiliza sus propias variables de entorno, Dockerfile y docker-compose.

#### Ejecutar

```bash
  docker-compose -f docker-compose.yml -f test.yml up
```

### Ambiente de producción

Utiliza sus propias variables de entorno, Dockerfile y docker-compose, se diferencia de los ambientes anteriores debido a que este ejecuta un build en su docker.

#### Ejecutar

```bash
  docker-compose -f docker-compose.yml -f production.yml up
```

### Docker

El proyecto cuenta con 3 archivos de docker y 3 de docker-compose. Asimismo cuenta con el nombre de la red, creada en la imagen del backend.

##### Desarrollo:

- Dockerfile
- docker-compose.yml

##### Testing:

- Dockerfile.test
- test.yml

##### Producción:

- Dockerfile.production
- production.yml

### Gitlab CI

El CI de gitlab se realiza automáticamente realizando un push al repositorio este ejecuta los siguientes pipelines:

- Instalación de dependencias
- Testing
- Eslint

Puede editar la configurar en el archivo .gitlab-ci.yml

### Variables de entorno

El nombre de las variables de entorno las puede encontrar el archivo: .env.example

### Consideraciones

- La red que se encuentra en el contenedor si se cambia el nombre del network se debe cambiar también en el frontend..
- El nombre del contenedor del frontend utiliza el nombre del contenedor del backend para conectarse, por tal motivo si se cambiar el nombré del contenedor backend se debe cambiar en el frontend.
  Si se actualiza el nombre del contenedor de la base de datos también se debe actualizar en la conexión database y backend.
