# Back-End

**Author:** Renny Petit

Project developed for the company Regin: https://www.reign.cl/es/

## Documentation

The project is developed with nodejs, nestjs and mongodb as database. Likewise, a docker image was created in which the containers of: Backend, Database, Frontend were connected.

In the same way, the deployment is carried out in gitlab, the CI of pipelines is run, the installation of the dependencies and the eslint are executed.

**URL**: https://gitlab.com/rennypetit/prueba-reign-backend

### Development environment

It uses its own environment variables, Dockerfile and docker-compose.

#### Run

```bash
 docker-compose up
```

### Testing environment

It uses its own environment variables, Dockerfile and docker-compose.

#### Run

```bash
  docker-compose -f docker-compose.yml -f test.yml up
```

### Production environment

It uses its own environment variables, Dockerfile and docker-compose, it differs from previous environments because it runs a build on your docker.

#### Run

```bash
  docker-compose -f docker-compose.yml -f production.yml up
```

###Docker

The project has 3 docker files and 3 docker-compose files. It also has the name of the network, created in the backend image.

##### Developing:

-Docker file

- docker-compose.yml

#####Testing:

-Dockerfile.test

- test.yml

##### Production:

- Dockerfile.production
  -production.yml

### Gitlab CI

The gitlab CI is done automatically by pushing to the repository, it executes the following pipelines:

- Installation of dependencies
  -Testing
- Eslint

You can edit the configure in the .gitlab-ci.yml file

### Environment Variables

The name of the environment variables can be found in the file: .env.example

### Considerations

- The network that is in the container if the name of the network is changed must also be changed in the frontend.
- The frontend container name uses the backend container name to connect, so if you change the backend container name you must change it in the frontend.
  If the database container name is updated it must also be updated in the database and backend connection.
