import { Module, Global } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

@Global()
@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: () => {
        return {
          uri: process.env.URI,
        };
      },
    }),
  ],
  exports: [MongooseModule],
})
export class DatabaseModule {}
