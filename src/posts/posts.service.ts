import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Post } from './entities/post.entity';

@Injectable()
export class PostsService {
  constructor(@InjectModel(Post.name) private postsModel: Model<Post>) {}
  async findAll() {
    return await this.postsModel
      .find({ publish: true })
      .sort({ _id: 1 })
      .exec();
  }

  async findAllInitial() {
    return await this.postsModel.count();
  }

  async findOne(objectID: string) {
    return this.postsModel.findOne({ objectID });
  }

  async create(content) {
    const newPost = new this.postsModel();
    newPost.content = content;
    newPost.publish = true;
    newPost.objectID = content.objectID;
    return await newPost.save();
  }
  async update(id: string) {
    const post = await this.postsModel
      .findByIdAndUpdate(id, { $set: { publish: false } }, { new: true })
      .exec();
    if (!post) {
      throw new NotFoundException(`Post #${id} not found`);
    }
    return post;
  }
}
