import { Test, TestingModule } from '@nestjs/testing';
import { PostsController } from './posts.controller';
import { PostsService } from './posts.service';

describe('aaPostsController', () => {
  let controller: PostsController;

  const mockPostsService = {
    findAll: jest.fn(),
    update: jest.fn().mockImplementation((id) => ({ id, publish: false })),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PostsController],
      providers: [PostsService],
    })
      .overrideProvider(PostsService)
      .useValue(mockPostsService)
      .compile();

    controller = module.get<PostsController>(PostsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should all posts', () => {
    expect(controller.findAll());
  });

  it('should update a post', () => {
    expect(controller.update('61f0b326ff306193f9dfe9f4')).toEqual({
      id: '61f0b326ff306193f9dfe9f4',
      publish: false,
    }),
      expect(mockPostsService.update).toHaveBeenCalled();
  });
});
