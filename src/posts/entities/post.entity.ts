import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Post extends Document {
  @Prop({ required: true, index: true })
  objectID: string;

  @Prop({ required: true, type: Object })
  content: string;

  // se utiliza para poder buscar de manera eficiente
  @Prop({ required: true })
  publish: boolean;
}
export const PostsSchema = SchemaFactory.createForClass(Post);
