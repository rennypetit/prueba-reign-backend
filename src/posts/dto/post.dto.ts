import { IsNotEmpty, IsBoolean, IsObject, IsMongoId } from 'class-validator';
export class CreatePostDto {
  @IsNotEmpty()
  @IsObject()
  readonly content: object;

  @IsNotEmpty()
  @IsBoolean()
  readonly publish: boolean;
}
export class UpdatePostDto {
  @IsNotEmpty()
  @IsMongoId()
  readonly id: string;
}
