import { Controller, Get, Patch, Param } from '@nestjs/common';
import { PostsService } from './posts.service';
import { MongoIdPipe } from '../common/mongo-id.pipe';

@Controller('posts')
export class PostsController {
  constructor(private readonly postsService: PostsService) {}

  @Get()
  findAll() {
    return this.postsService.findAll();
  }

  @Patch(':id')
  update(@Param('id', MongoIdPipe) id: string) {
    return this.postsService.update(id);
  }
}
