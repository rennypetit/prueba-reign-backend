import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { PostsService } from './posts/posts.service';
@Injectable()
export class AppService {
  constructor(
    private httpService: HttpService,
    private postsService: PostsService,
  ) {
    this.createInitial();
  }

  private readonly logger = new Logger(AppService.name);

  //cuando la base de datos esta vacia
  async createInitial(): Promise<void> {
    // si es mayor a 0 quiere decir que es la primera vez
    if ((await this.postsService.findAllInitial()) === 0) {
      const response = await this.httpService
        .get(process.env.API_TEST)
        .toPromise();
      response.data.hits.map((post) => {
        this.postsService.create(post);
      });
    }
  }

  @Cron(CronExpression.EVERY_HOUR)
  async handleCron() {
    const response = await this.httpService
      .get(process.env.API_TEST)
      .toPromise();
    for (let i = 0; i < response.data.hits.length; i++) {
      const post = await this.postsService.findOne(
        response.data.hits[i].objectID,
      );
      if (!post) {
        // si el post no se encuentra quiere decir que es uno nuevo
        this.postsService.create(response.data.hits[i]);
      } else {
        // si se encuentra quiere decir que este fue el ultimo insertado y los siguientes ya estan
        break;
      }
    }
  }
}
