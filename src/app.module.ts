import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { HttpModule, HttpService } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';

import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
import { PostsModule } from './posts/posts.module';
import { environments } from './environments';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: environments[process.env.NODE_ENV] || '.env',
      isGlobal: true,
    }),
    DatabaseModule,
    HttpModule,
    ScheduleModule.forRoot(),
    PostsModule,
  ],

  providers: [
    AppService,
    {
      provide: 'POSTS',
      useFactory: async (http: HttpService) => {
        const response = await http.get(process.env.API_TEST).toPromise();
        return response.data;
      },
      inject: [HttpService],
    },
  ],
})
export class AppModule {}
