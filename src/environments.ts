export const environments = {
  dev: '.env',
  test: '.env.test',
  production: '.env.production',
};
